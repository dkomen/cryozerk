/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

use serde::{Serialize, Deserialize};
use mongodb::{bson,bson::{doc, Bson}};
use futures::stream::{StreamExt};

const COLLECTION_NAME: &str = "client_files";

#[derive(Serialize, Deserialize, Debug)]
struct FilesDocument {
    #[serde(rename = "_id", skip_serializing_if = "Option::is_none")]
    id: Option<bson::oid::ObjectId>,
    access_key: String,
    registration_access_key: String,
    unique_code: String,
    description: String,
    layer: String,
    verification_key: String,
    protected: String,
    data: String
}

/// Get all the files data from MongoDB
pub async fn get_all() -> Result<Vec<crate::structs::Data>, String> {
    //println!("Connecting: {}", lib_cryozerk_streams::CONFIGURATION_SERVER.mongodb_connection_string);

    let result_database_connect = super::connect().await;
    
    if result_database_connect.is_ok() {
        let database = result_database_connect.unwrap();
        let client_files = database.collection(COLLECTION_NAME);
        
        let result_cursor = futures::executor::block_on(client_files.find(None, None));
        if result_cursor.is_ok() {
            let mut cursor = result_cursor.unwrap();
            let mut data: Vec<crate::structs::Data> = vec![];
            while let Some(data_item) = cursor.next().await {
                let result_retreived_data = bson::from_bson::<FilesDocument>(Bson::Document(data_item.unwrap()));
                if result_retreived_data.is_ok() {
                    let retreived_data = result_retreived_data.unwrap();
                    let deserialised: crate::structs::Data = serde_json::from_str(&retreived_data.data).unwrap();
                    data.push(deserialised);
                } else {
                    return Err("Error retrieving from database".into());
                }
            }

            Ok(data)
        } else {

            Err(format!("Database error: {}", result_cursor.unwrap_err()))
        }
    } else {
        Err(format!("Database error: {}", result_database_connect.unwrap_err()))    
    }
}

/// Add file to MongoDB
pub async fn add(new_data: &crate::structs::Data) -> Result<(), String> {
    let database = tokio::spawn(async move {
        super::connect().await.unwrap()
    }).await.unwrap();
    let client_files = database.collection(COLLECTION_NAME);
    let serialised = serde_json::to_string(&new_data).unwrap();
    let result_serialised_data =  bson::to_bson(&serialised);
    if result_serialised_data.is_ok() {
        let files_document = FilesDocument {
            id: None,
            access_key: std::str::from_utf8(&new_data.access_key).unwrap().into(),
            registration_access_key: std::str::from_utf8(&new_data.registration_access_key).unwrap().into(),
            unique_code: std::str::from_utf8(&new_data.unique_code).unwrap().into(),
            verification_key: new_data.verification_key.to_owned(),
            protected: new_data.protected_key.to_owned(),
            description: new_data.description.to_owned(),
            layer: new_data.layer.to_owned(),
            data: serialised
        };
        let result_serialised_data = bson::to_bson(&files_document);
        if result_serialised_data.is_ok() {
            let serialised_data = result_serialised_data.unwrap();
            let option_document = serialised_data.as_document();
            if option_document.is_some() && client_files.insert_one(option_document.unwrap().to_owned(), None).await.is_ok() {
                return Ok(())
            } else {
                return Err("Error adding data to persisted storage".into());
            };
        } else {
            return Err("Error adding data to persisted storage".into());
        }
    } else {
        Err(format!("Error adding data to persisted storage: {:?}", result_serialised_data.unwrap_err()))
    }
}

/// Delete a file from MongoDB and disk
pub async fn delete(access_key: &Vec<u8>, unique_code: &Vec<u8>, verification_key: &str) -> bool {
    let database = tokio::spawn(async move {
        super::connect().await.unwrap()
    }).await.unwrap();
    let client_files = database.collection(COLLECTION_NAME);
    let mut document_filter = doc! { 
        "verification_key": String::from(verification_key),
        "access_key": std::str::from_utf8(&access_key).unwrap(),
        "unique_code": std::str::from_utf8(&unique_code).unwrap()
    };
    if verification_key.len() <=2  {
        crate::log!("No verification_key");
        document_filter = doc! { 
            "access_key": std::str::from_utf8(&access_key).unwrap(),
            "unique_code": std::str::from_utf8(&unique_code).unwrap()
        }
    }

    let result = client_files.delete_one(document_filter,None).await;
    if result.is_err() || result.unwrap().deleted_count == 0 {
        crate::cz::log!("Could not delete file from data store");
        return false;
    }

    true
}