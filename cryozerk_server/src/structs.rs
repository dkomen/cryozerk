/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/
use serde::{Serialize, Deserialize};
use chrono::{DateTime, Local};
use lib_cryozerk_streams as cz;

/// Contains the data a client has uploaded and is used for later retrieval
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Data {
    /// The timestamp of when the data was uploaded to the server
    pub creation_time_stamp: DateTime<Local>,
    /// The initial handshake of this data from which we can get encryption type, client_id etc
    pub handshake: cz::structs::InitialHandshake,
    /// The access key required to gain access to this data item. This key and the unique_code together define the access key of the this data.
    pub access_key: Vec<u8>,
    pub registration_access_key: Vec<u8>,
    /// A code that uniquely identifies this data and is what is needed to retrieve or drop this data again
    pub unique_code: Vec<u8>,
    /// This is just the access_key scrambled by the client with the users password.
    /// This used to prevent an entire download if the password is wrong as the server does not know what the password is. 
    /// The access_key used to encrypt must be the appended to maximum length instance and not the short version in the client configuration
    pub verification_key: String,
    /// An additional verification key used to allow downloads but without a simultanious delete. Only delete if the protected key matches
    pub protected_key: String,
    /// Should the data be automatically deleted on the server on retrieval or should it remain on the server until an explicit drop request was received?
    pub drop_on_retrieval: bool,
    /// The type of data
    pub data_type: lib_cryozerk_streams::enums::PacketType,
    /// The length of the actual data bytes
    pub data_packet_bytes_length: u64,
    /// The name of the file written to disk, this is not the name of the original file the client uploaded but rather the unique_id based one
    pub file_name: String,
    /// A description for this data
    pub description: String,
    pub layer: String
}
impl Data {
    /// Create a new instance and persist the file bytes to disk if so indicated
    pub fn new(layer: &str, description: &str, creation_time_stamp: &DateTime<Local>, access_key: &Vec<u8>, registration_access_key: &Vec<u8>, unique_code: &Vec<u8>, verification_key: &Vec<u8>, protected_key: &Vec<u8>, data_type: &cz::enums::PacketType, 
      drop_on_retrieval: &bool, handshake: &lib_cryozerk_streams::structs::InitialHandshake, data_packet_bytes: &Vec<u8>) -> Result<crate::structs::Data, String> {

        let mut handshake_to_persist = handshake.clone();
        handshake_to_persist.packet_symmetric_encryption_key.clear(); //The symmetric encryption does nothing for us if we save it, so clear it to save space

        //let data_bytes = data_packet_bytes.to_owned();
        let file_name_with_path = lib_cryozerk_streams::files::get_path_to_data_file(&unique_code, &access_key)?;
        //let _ = Data::persist_data_packet_bytes(&file_name_with_path, &data_bytes).to_owned()?;
        let serialised_verification_key = serde_json::to_string(&cz::general::strip_zeros(&verification_key.to_owned())).unwrap();
        let mut serialised_protected_key = serde_json::to_string(&cz::general::strip_zeros(&protected_key.to_owned())).unwrap();
        
        // This is a quick fix because an empty array is serialised to "[]"
        if serialised_protected_key == String::from("[]") { serialised_protected_key = String::new() };

        Ok(crate::structs::Data {
          creation_time_stamp: *creation_time_stamp,
          access_key: access_key.to_owned(),
          registration_access_key: registration_access_key.to_owned(),
          verification_key: serialised_verification_key,
          protected_key: serialised_protected_key,
          data_type: data_type.to_owned(),
          drop_on_retrieval: *drop_on_retrieval,
          unique_code: unique_code.to_owned(),
          handshake: handshake_to_persist,
          file_name: file_name_with_path,
          description: description.to_owned(),
          layer: layer.to_owned(),
          data_packet_bytes_length: data_packet_bytes.len() as u64
        })
    }

    /// Persist the bytes to a file on disk creating the path if needed
    pub fn persist_data_packet_bytes(file_name_with_path: &str, data_packet_bytes: &Vec<u8>) -> Result<(), String> {
        let result_write = super::cz::files::write_to_file(&file_name_with_path, &data_packet_bytes, true);
        if !result_write {
          super::cz::log!("Error writing to file", file_name_with_path);
          return Err("Could not persist the file contents".into());
        }

        Ok(())
    }
}