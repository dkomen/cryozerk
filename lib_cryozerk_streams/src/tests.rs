// #![feature(async_await)]
// use tokio;
// use std::sync::Mutex;
// use std::net::IpAddr;
// use crate::structs;
// use crate::general;

// pub static MESSAGE_HANDLERS: crate::structs::callbacks::MessageHandlers = structs::callbacks::MessageHandlers {
//     on_new_connection: on_new_connection, 
//     on_arrived: on_arrived,
//     on_retrieve:on_retrieve,
//     on_packet_details: on_packet_details,
//     on_delete: on_delete,
//     on_timedout_delete: on_timedout_delete,
//     on_message: on_message,
//     on_list_uploads: on_list_uploads
// };

// pub static BANNING_HANDLERS : structs::callbacks::BanningHandlers = structs::callbacks::BanningHandlers {
//     on_update_ban_details: crate::tests::banning::on_update_ban_details,
//     on_check_if_banned: crate::tests::banning::on_check_if_banned
// };

// lazy_static! { 
//     pub static ref BANNED: std::sync::Mutex<Vec<structs::banning::Ban>> = std::sync::Mutex::new(vec![]);
//     pub static ref DELAY_PACKETS_BY: std::time::Duration = std::time::Duration::new(0, crate::CONFIGURATION_SERVER.delay_by_nano_seconds as u32);
// }


// #[tokio::main]
// #[test]
// async fn create_server() {
//     // let result_start = crate::server::start_new_server(&MESSAGE_HANDLERS, &BANNING_HANDLERS, on_benchmarks_handler, Some(*DELAY_PACKETS_BY)).await;

//     // assert!(result_start.is_ok(), result_start.unwrap_err())
// }

// /// A new connection from a client
// pub fn on_new_connection(initial_handshake: &structs::InitialHandshake) -> std::result::Result<(structs::configuration::ServerAccess, usize), std::string::String> {
//     let option_server_access = crate::CONFIGURATION_SERVER.access_keys.iter().filter(|ak| 
//         general::append_vector_to_length(ak.access_key.as_bytes().to_vec(), crate::MAXIMUM_LENGTH_ACCESS_KEY) == initial_handshake.access_key).nth(0);

//     assert!(option_server_access.is_some(), "access_key does not have access to the server");

//     let server_access=option_server_access.unwrap().clone();

//     Ok((server_access, 0))
// }

// fn on_arrived(data_packet: structs::DataPacket, initial_handshake: structs::InitialHandshake) -> Result<String, String> {
//     Ok("".into())
// }

// fn on_retrieve(new_packet_symmetric_encryption_key: Vec<u8>, access_key: Vec<u8>, unique_code: Vec<u8>, verification_key: Vec<u8>) -> Result<Vec<u8>, String> {
//     Ok("".into())
// }

// fn on_list_uploads(access_key: &Vec<u8>, layer: &Vec<u8>) -> Result<structs::packet_data_types::ListUploads, String> {
//     Err("Un tested".into())
// }

// fn on_packet_details(access_key: Vec<u8>, unique_code: Vec<u8>) -> Result<structs::packet_data_types::PacketDetails, String> {
//     Err("".into())
// }

// fn on_delete(access_key: Vec<u8>, unique_code: Vec<u8>, verification_key: Vec<u8>) -> Option<String> {
//     Some("Done".into())
// }

// fn on_timedout_delete(access_key: Vec<u8>, timeout_seconds: &i64) -> Option<String> {
//     Some("Done".into())
// }

// fn on_message(_access_key: &Vec<u8>, message: &structs::packet_data_types::Message) -> Result<(String, Vec<u8>), String> {
//     Ok(("Done".into(), vec![]))
// }

// pub fn on_benchmarks_handler(_benchmarks_result: structs::BenchmarkResult) {}

// mod banning {
//     use std::net::IpAddr;
//     /// A failed connection was attempted so check to see if an ip address or access key is in the ban list and update its details accordingly
//     pub fn on_update_ban_details(ip_address: &IpAddr, access_key: &Option<Vec<u8>>) { }

//     /// Check if a ban has been applied to the current Ip address or access_key
//     pub fn on_check_if_banned(ip_address: &IpAddr, access_key: &Option<Vec<u8>>) -> bool {
//         false
//     }
// }
// #[cfg(test)]
// pub mod integration_tests {
//     pub mod struct_serialisation {
//         use crate::structs;
//         use crate::enums;
//         use crate::general;
//         use crate::encryption;

//         const access_key: &str = "AccessKey";

//         //#[test]
//         fn initial_handshake() {
//             let symmetric_encryption_key = encryption::generate_random_vector(crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY);
//             let object = structs::InitialHandshake::new_session(
//                 access_key, "PrivateAccess", 
//                 &enums::EncryptionScheme::Rsa4096Aes256, 
//                 &enums::EnableCompression::Yes, 
//                 &symmetric_encryption_key
//             );

//             let result_serialised = object.into_bytes(crate::tests::on_benchmarks_handler);
//             assert!(result_serialised.is_ok(), format!("Could not serialise: {}", result_serialised.unwrap_err()));
//             let serialised = result_serialised.unwrap();
//             let result_deserialised = structs::InitialHandshake::from_bytes(&serialised, crate::tests::on_benchmarks_handler);
//             assert!(result_deserialised.is_ok(), format!("Could not deserialise: {}", result_deserialised.unwrap_err()));

//             let desialised = result_deserialised.unwrap();
//             assert!(object.access_key==desialised.access_key, "1");
//             assert!(object.enable_compression==desialised.enable_compression, "2");
//             assert!(object.encryption_scheme==desialised.encryption_scheme, "3");
//             assert!(object.packet_symmetric_encryption_key==desialised.packet_symmetric_encryption_key, "4");
//             assert!(object.status==desialised.status);
//         }

//         pub mod packet_data_types {
//             use crate::structs;
//             use crate::enums;
//             use crate::general;
//             use crate::encryption;
//         }
//     }

// }

#[allow(unused_imports)]
use super::encryption as enc;

/// Does encryption and compression of a data packet with File data work
#[test]
pub fn master_packet_serialisation() {
    // let allow_compression = crate::enums::EnableCompression::No;
    // let symmetric_key = crate::encryption::generate_random_vector(crate::LENGTH_OF_SYMMETRIC_ENCRYPTION_KEY as u32);
    // let bytes = crate::files::read_from_file("/home/dean/Pictures/Linux2Percent_1920x1080.jpg").unwrap();
    // let processed_bytes = crate::encryption::process_file_bytes(false, &mut bytes.to_owned(), "SuperSecr1T1FENT3y".as_bytes().to_vec(), crate::enums::EncryptionScheme::Rsa4096Aes256, &allow_compression, on_benchmarks_handler).unwrap();
    // let verification_key = crate::general::append_vector_to_length("AccessKey".as_bytes().to_vec(), crate::MAXIMUM_LENGTH_ACCESS_KEY);
    // let protected_key = crate::general::append_vector_to_length(vec![], crate::MAXIMUM_LENGTH_ACCESS_KEY);
    // let mut file = crate::structs::packet_data_types::File::new(&processed_bytes, "filename", "", "", false, &verification_key, &vec![], &protected_key);
    // let serialised = file.into_bytes().unwrap();
    // let send_data_packet = crate::structs::DataPacket::new(&crate::enums::PacketType::File, &serialised, symmetric_key.clone()).unwrap();

    // let receive_data_packet = crate::structs::DataPacket::from_bytes(&send_data_packet, symmetric_key.clone()).unwrap();

    // let file2 = crate::structs::packet_data_types::File::from_bytes(receive_data_packet.data).unwrap();
    // let deprocessed_bytes = crate::encryption::process_file_bytes(true, &mut file2.bytes.to_owned(), "SuperSecr1T1FENT3y".as_bytes().to_vec(), crate::enums::EncryptionScheme::Rsa4096Aes256, &allow_compression, on_benchmarks_handler).unwrap();
    // assert!(bytes==deprocessed_bytes);
}

pub fn on_benchmarks_handler(_benchmarks_result: crate::structs::BenchmarkResult) {}