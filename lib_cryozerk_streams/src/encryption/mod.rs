/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! All encryption (RSA and AES) and compression functions reside here

use aesstream::{AesReader, AesWriter};
use crypto::aessafe::{AesSafe128Decryptor, AesSafe128Encryptor, AesSafe256Decryptor, AesSafe256Encryptor};
use crypto::scrypt::ScryptParams;
use rand::random;
use rand_core::{OsRng, RngCore};
use rsa::{PaddingScheme, PublicKey, RSAPrivateKey, RSAPublicKey};
use std::io::{Cursor, Read, Write};

use sha2::{Sha512, Digest};

const SCRYPT_PARAM_LOG_N: u8 = 8;
const SCRYPT_PARAM_R: u32 = 2;
const SCRYPT_PARAM_P: u32 = 2;

use super::structs;
use super::encryption;
use super::enums;
use super::log;

/// Generate a sha512 digest of the given bytes
pub fn generate_sha_512_hash(bytes: &Vec<u8>) -> Vec<u8> {

    let mut hasher = Sha512::new();

    // println!("Started benchmark...");
    // let start = std::time::Instant::now();
    // let bytes = option_file_bytes.clone().unwrap();
    //let bytes_len = bytes.len();
    hasher.update(bytes);
    // read hash digest and consume hasher
    let result = hasher.finalize();

    result[..].into()

    // let elapsed = std::time::Duration::from_millis(start.elapsed().as_millis() as u64);

    // println!("Result: {:.2}MiB/s", (bytes_len as f32/(elapsed.as_millis() as f32)/1024.0));

    // return Err("An error occured when trying to access the file".into());
}

/// Do any processing on bytes bytes before being sent or after receiving
pub fn process_file_bytes<'a>(is_arriving_data: bool, bytes: &mut Vec<u8>, user_password: Vec<u8>, encryption_scheme: enums::EncryptionScheme, enable_compression: &enums::EnableCompression, on_benchmarks_handler: fn(benchmarks: structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
    let mut processed_bytes: Vec<u8> = bytes.clone(); // ! Come look here again... mega RAM usage

    if is_arriving_data {
        // Decryption
        let mut obfuscated_blocks = crate::structs::bytes_processing::ProcessedBlocks::from_bytes(&mut processed_bytes)?;
        
        processed_bytes = obfuscated_blocks.decrypt(encryption_scheme, user_password, on_benchmarks_handler)?;

        // Decompression
        if *enable_compression == super::enums::EnableCompression::Yes {
            let result_new_processed_bytes = enums::EnableCompression::decompress_bytes(&processed_bytes, on_benchmarks_handler);
            if result_new_processed_bytes.is_ok() {
                processed_bytes.truncate(0);
                processed_bytes = result_new_processed_bytes.unwrap();
            } else {
                return result_new_processed_bytes;
            }
        };
    } else {
        // Compression
        if *enable_compression == super::enums::EnableCompression::Yes {
            let result_new_processed_bytes = enums::EnableCompression::compress_bytes(&processed_bytes, on_benchmarks_handler);
            if result_new_processed_bytes.is_ok() {
                processed_bytes.truncate(0);
                processed_bytes = result_new_processed_bytes.unwrap();
            } else {
                return result_new_processed_bytes;
            }
        };

        // Encryption
        let result_encrytpion = crate::structs::bytes_processing::ProcessedBlocks::encrypt(encryption_scheme, &mut processed_bytes, user_password.clone(), on_benchmarks_handler);
        if result_encrytpion.is_ok() {
            let mut processed_blocks = result_encrytpion.unwrap();
            let encrypted_bytes = processed_blocks.into_bytes();
            processed_bytes = if encrypted_bytes.is_ok() {
                let mut bytes: Vec<u8> = vec![];
                bytes.append(&mut processed_blocks.salt);
                bytes.append(&mut encrypted_bytes.unwrap());
                bytes
            } else {
                return encrypted_bytes;
            };
        }
    };

    Ok(processed_bytes)
}

/// A quick encryption scheme to obfuscate a set of bytes
// pub fn scrambler(scramble: &bool, to_scramble: Vec<u8>, password_key: Vec<u8>) -> Vec<u8> {
//     const CONST_SIZE: usize = 1024*512;
//     if password_key.len() == 0 {
//         return to_scramble;
//     } else {
//         let mut scrambled: Vec<u8> = to_scramble.clone();
//         let mut reversed_password_key: Vec<u8> = password_key.clone();
//         let length_to_scramble = {
//             if scrambled.len() < CONST_SIZE + password_key.len() * ((password_key[0] as usize + reversed_password_key[0] as usize )*2 + 1) {
//                 scrambled.len()
//             } else {
//                 CONST_SIZE + password_key.len() * ((password_key[0] as usize + reversed_password_key[0] as usize )*2 + 1)
//             }
//         };
//         reversed_password_key.reverse();

//         let mut password_b: usize = 0;
//         for b in 0..length_to_scramble {
//             if password_b == password_key.len()-1 {
//                 password_b = 0;
//             }
//             match *scramble {
//                 true => scrambled[b] = scrambled[b].rotate_left(password_key[password_b] as u32 + reversed_password_key[password_b] as u32),
//                 false => scrambled[b] = scrambled[b].rotate_right(password_key[password_b] as u32 + reversed_password_key[password_b] as u32)
//             }
//             password_b +=1;
//         }

//         return scrambled;
//     };
// }
pub fn scrambler(scramble: &bool, to_scramble: Vec<u8>, password_key: Vec<u8>, actual_data_len: usize) -> Vec<u8> {
    const CONST_SIZE: usize = 1024*512;
    //let actual_data_len = to_scramble.len(); //crate::general::strip_zeros(&to_scramble).len();
    if password_key.len() == 0 {
        return to_scramble;
    } else {
        let mut scrambled: Vec<u8> = to_scramble.clone();
        let length_to_scramble = {
            if scrambled.len() < CONST_SIZE + password_key.len() * ((password_key[0] as usize)*2 + 1) {
                scrambled.len()
            } else {
                CONST_SIZE + password_key.len() * ((password_key[0] as usize)*2 + 1)
            }
        };

        let mut password_b: usize = 0;
        let mut rotater_count: u32 = 1;
        for b in 0..length_to_scramble {
            if b % 25 == 0 {
                rotater_count+=1;
                if rotater_count > 10 {
                    rotater_count = 1;
                }
            }
            if password_b == password_key.len()-1 {
                password_b = 0;
            }
            match *scramble {
                true => { scrambled[b] = scrambled[b] ^ password_key[password_b] as u8; scrambled[b] = scrambled[b].rotate_left(rotater_count) as u8 },
                false => { scrambled[b] = scrambled[b].rotate_right(rotater_count); scrambled[b] = password_key[password_b] as u8 ^ scrambled[b] }
            }
            password_b +=1;
        }

        //Set unused bytes to zero again
        for b in actual_data_len..scrambled.len() {
            scrambled[b] = 0;
        }

        return scrambled;
    };
}

/// Get a vector of randomly generated bytes
pub fn generate_random_vector(length: u32) -> Vec<u8> {
    let mut random_vector: Vec<u8> = vec![];
    for _ in 0..length {
        let mut random_byte = random::<u8>();
        if random_byte == 0 { random_byte = random::<u8>(); if random_byte==0 { random_byte=127; } }; // This check is adequate, removing 0 is done for the scrambler function
        random_vector.push(random_byte)
    }

    random_vector
}

/// Get a randomly generated vector of given length and encrypted with the servers RSA public key if needed
pub fn get_random_key(id: &u16, length: u16, encryption_scheme: &enums::EncryptionScheme) -> Result<Vec<u8>, String> {
    let random_key = generate_random_vector(length as u32);

    let option_random_key = match encryption_scheme {
        enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Rsa2048Aes128 => {
            let rsa_key_pair = &crate::RSA_KEY_PAIRS_CLIENT.get_key_pair(&id)?.rsa_2048_key_pair;
            let rsa_public_key = encryption::rsa_get_public_key(&rsa_key_pair.public_key);
            let option = encryption::rsa_encrypt_public(&rsa_public_key, &random_key);

            option
        }
        enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa4096Aes128 => {
            let rsa_key_pair = &crate::RSA_KEY_PAIRS_CLIENT.get_key_pair(&id)?.rsa_4096_key_pair;
            let rsa_public_key = encryption::rsa_get_public_key(&rsa_key_pair.public_key);

            encryption::rsa_encrypt_public(&rsa_public_key, &random_key)
        }
        _ => Some(random_key),
    };
    if option_random_key.is_some() {
        Ok(option_random_key.unwrap())
    } else {
        Err("Could not generate random key".into())
    }
}

/// Encrypt a vector to an AES encrypted vector - timed
pub fn aes_encrypt_timed(encryption_scheme: enums::EncryptionScheme, to_encrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>, on_benchmarks_handler: fn(structs::BenchmarkResult)) -> Option<Vec<u8>> {
    if to_encrypt.len() == 0 {
        return Some(vec![]);
    }
    let option_kill_sender = structs::Benchmarks::show_predicted_progress(&crate::LABEL_ENCRYPTING, to_encrypt.len() as f32, crate::BENCHMARKS.lock().unwrap().aes_256_bytes_per_second);
    let start = std::time::Instant::now();

    let option_encrypted =  match encryption_scheme {
        enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Aes256 => aes_256_encrypt(&to_encrypt, &salt, &password_key),
        enums::EncryptionScheme::Rsa4096Aes128 | enums::EncryptionScheme::Rsa2048Aes128 | enums::EncryptionScheme::Aes128 => aes_128_encrypt(&to_encrypt, &salt, &password_key),
        _ => Some(to_encrypt.clone())
    };

    if option_kill_sender.is_some() {
        let _ = option_kill_sender.unwrap().send(true);
    };
    
    if option_encrypted.is_some() {
        let result = structs::BenchmarkResult::new(enums::BenchmarkType::Encryption, enums::EncryptionScheme::Aes256 as u8, to_encrypt.len(), start.elapsed());
        on_benchmarks_handler(result);
        return option_encrypted;
    } else {
        return None;
    }
}

/// Encrypt a vector to an AES256 encrypted vector
pub fn aes_256_encrypt(to_encrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>) -> Option<Vec<u8>> {
    if to_encrypt.len() == 0 {
        return Some(vec![]);
    }

    let params = ScryptParams::new(SCRYPT_PARAM_LOG_N, SCRYPT_PARAM_R, SCRYPT_PARAM_P);
    let mut output_key = [0u8; 32];
    crypto::scrypt::scrypt(&password_key, &salt, &params, &mut output_key);
    let mut key = [0u8; 32];
    OsRng.fill_bytes(&mut key);
    let encryptor = AesSafe256Encryptor::new(&output_key);
    let mut is_encrypted = false;
    let mut encrypted = Vec::new();
    {
        let result_writer = AesWriter::new(&mut encrypted, encryptor);
        if result_writer.is_ok() {
            let mut writer = result_writer.unwrap();
            let result_write = writer.write_all(to_encrypt);
            if result_write.is_ok() {
                is_encrypted = true;
            }
        }
    }

    if is_encrypted {
        return Some(encrypted);
    } else {
        return None;
    }
}

/// Encrypt a vector to an AES128 encrypted vector
pub fn aes_128_encrypt(to_encrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>) -> Option<Vec<u8>> {
    if to_encrypt.len() == 0 {
        return Some(vec![]);
    }

    let params = ScryptParams::new(SCRYPT_PARAM_LOG_N, SCRYPT_PARAM_R, SCRYPT_PARAM_P);
    let mut output_key = [0u8; 16];
    crypto::scrypt::scrypt(&password_key, &salt, &params, &mut output_key);
    let mut key = [0u8; 32];
    OsRng.fill_bytes(&mut key);
    let encryptor = AesSafe128Encryptor::new(&output_key);
    let mut is_encrypted = false;
    let mut encrypted = Vec::new();
    {
        let result_writer = AesWriter::new(&mut encrypted, encryptor);
        if result_writer.is_ok() {
            let mut writer = result_writer.unwrap();
            let result_write = writer.write_all(to_encrypt);
            if result_write.is_ok() {
                is_encrypted = true;
            }
        }
    }

    if is_encrypted {        
        return Some(encrypted);
    } else {
        return None;
    }
}

/// Decrypt an AES encrypted vector
pub fn aes_decrypt_timed(encryption_scheme: enums::EncryptionScheme, to_decrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>, on_benchmarks_handler: fn(structs::BenchmarkResult)) -> Option<Vec<u8>> { // ! change the salt
    if to_decrypt.len() == 0 {
        return Some(vec![]);
    }
    let option_kill_sender = structs::Benchmarks::show_predicted_progress(&crate::LABEL_DECRYPTING, to_decrypt.len() as f32, crate::BENCHMARKS.lock().unwrap().aes_256_bytes_per_second);
    let start = std::time::Instant::now();

    let option_decrypted =  match encryption_scheme {
        enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Aes256 => aes_256_decrypt(&to_decrypt, &salt, &password_key),
        enums::EncryptionScheme::Rsa4096Aes128 | enums::EncryptionScheme::Rsa2048Aes128 | enums::EncryptionScheme::Aes128 => aes_128_decrypt(&to_decrypt, &salt, &password_key),
        _ => Some(to_decrypt.clone())
    };

    if option_decrypted.is_some() {
        let result = structs::BenchmarkResult::new(enums::BenchmarkType::Encryption, enums::EncryptionScheme::Aes256 as u8, to_decrypt.len(), start.elapsed());
        on_benchmarks_handler(result);
    } 

    if option_kill_sender.is_some() {
        let _ = option_kill_sender.unwrap().send(true);
    };

    option_decrypted
}

/// Decrypt an AES256 encrypted vector
pub fn aes_256_decrypt(to_decrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>) -> Option<Vec<u8>> { // ! change the salt
    if to_decrypt.len() == 0 {
        return Some(vec![]);
    }
    
    let params = ScryptParams::new(SCRYPT_PARAM_LOG_N, SCRYPT_PARAM_R, SCRYPT_PARAM_P);
    let mut output_key = [0u8; 32];
    crypto::scrypt::scrypt(&password_key, &salt, &params, &mut output_key);

    let decryptor = AesSafe256Decryptor::new(&output_key);
    let result_reader = AesReader::new(Cursor::new(to_decrypt), decryptor);

    if result_reader.is_ok() {
        let mut reader = result_reader.unwrap();
        let mut decrypted = vec![];
        let result_read = reader.read_to_end(&mut decrypted);

        if result_read.is_ok() {
            return Some(decrypted);
        } else {
            log!(&format!("{}", result_read.unwrap_err()));
            return None;
        }
    } else {
        return None;
    }
}

/// Decrypt an AES128 encrypted vector
pub fn aes_128_decrypt(to_decrypt: &Vec<u8>, salt: &Vec<u8>, password_key: &Vec<u8>) -> Option<Vec<u8>> {
    let mut return_option: Option<Vec<u8>> = None;
    if to_decrypt.len() == 0 {
        return Some(vec![]);
    }

    let params = ScryptParams::new(SCRYPT_PARAM_LOG_N, SCRYPT_PARAM_R, SCRYPT_PARAM_P);
    let mut output_key = [0u8; 16];
    crypto::scrypt::scrypt(&password_key, &salt, &params, &mut output_key);

    let decryptor = AesSafe128Decryptor::new(&output_key);
    let result_reader = AesReader::new(Cursor::new(to_decrypt), decryptor);

    if result_reader.is_ok() {
        let mut reader = result_reader.unwrap();
        let mut decrypted = vec![];
        let result_read = reader.read_to_end(&mut decrypted);

        if result_read.is_ok() {
            return_option = Some(decrypted);
        } else {
            log!(&format!("{}", result_read.unwrap_err()));
        }
    }
    
    return_option
}

/// Generate a new RSA private and public key pair with an assigned name
pub fn rsa_generate_key_pair(key_name: &str, length: u16) -> super::structs::encryption::RSAKeyPair {
    crate::display!(&format!("Generating RSA{} key pair...", length));
    let mut rsa_key_pair = super::structs::encryption::RSAKeyPair {
        name: String::new(),
        private_key: vec![],
        public_key: vec![],
    };

    let mut rng = OsRng;
    let bits = length; //Recommend 2048+
    let private_key = RSAPrivateKey::new(&mut rng, bits as usize).expect("Failed to generate a RSA key");

    let pkcs8_encoded_private = rsa_export::pkcs8::private_key(&private_key).unwrap();
    let pkcs8_encoded_public = rsa_export::pkcs8::public_key(&private_key.to_public_key()).unwrap();
    rsa_key_pair.name = String::from(key_name);
    rsa_key_pair.private_key = pkcs8_encoded_private;
    rsa_key_pair.public_key = pkcs8_encoded_public;

    log!("Done generating RSA key pair");
    rsa_key_pair
}

/// Get the RSA Private key from a vector
pub fn rsa_get_private_key(pkcs8_encoded_private: &Vec<u8>) -> RSAPrivateKey {
    
    RSAPrivateKey::from_pkcs8(&pkcs8_encoded_private).expect("Could not create RSA private key")
}

/// Get the RSA Public key from a vector
pub fn rsa_get_public_key(pkcs8_encoded_public: &Vec<u8>) -> RSAPublicKey {    

    RSAPublicKey::from_pkcs8(&pkcs8_encoded_public).expect("Could not create RSA public key")
}

/// Encrypt a vector using a RSA public Key
pub fn rsa_encrypt_private(private_key: &RSAPrivateKey, to_encrypt: &Vec<u8>) -> Option<Vec<u8>> {
    let mut rng = OsRng;
    let padding = PaddingScheme::new_oaep::<sha2::Sha256>();

    let encrypted_data = private_key.encrypt(&mut rng, padding, &to_encrypt);
    if encrypted_data.is_ok() {
        Some(encrypted_data.unwrap())
    } else {
        log!(&format!("RSA Private key encryption error: {:#?}", encrypted_data));
        None
    }
}

/// Encrypt a vector using a RSA public Key
pub fn rsa_encrypt_public(public_key: &RSAPublicKey, to_encrypt: &Vec<u8>) -> Option<Vec<u8>> {
    let mut rng = OsRng;
    let padding = PaddingScheme::new_oaep::<sha2::Sha256>();

    let encrypted_data = public_key.encrypt(&mut rng, padding, &to_encrypt);
    if encrypted_data.is_ok() {
        Some(encrypted_data.unwrap())
    } else {
        log!("RSA Public key encryption error", encrypted_data.unwrap_err());

        None
    }
}

/// Decrypt a vector using a RSA Private Key
pub fn rsa_decrypt_private(private_key: &RSAPrivateKey, to_decrypt: &Vec<u8>) -> Option<Vec<u8>> {
    let padding = PaddingScheme::new_oaep::<sha2::Sha256>();
    let decrypted_data = private_key.decrypt(padding, &to_decrypt);

    if decrypted_data.is_ok() {
        Some(decrypted_data.unwrap())
    } else {
        log!("RSA Private key decryption error", decrypted_data);

        None
    }
}