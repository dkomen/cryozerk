/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! All enums of the CryoZerk processing centre are kept here

use flate2::write::ZlibDecoder;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use serde_repr::*;
use std::io::prelude::*;

use super::encryption;
use super::structs;
use super::enums;


#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u16)]
pub enum RsaKeyLength {
    RSA4096 = 4096,
    RSA2048 = 2048,
    Unknown = 0,
}
impl RsaKeyLength {
    /// Convert a u16 into a named instance
    pub fn from(number: u16) -> RsaKeyLength {
        match number {
            4096 => RsaKeyLength::RSA4096,
            2048 => RsaKeyLength::RSA2048,
            _ => RsaKeyLength::Unknown,
        }
    }
}

/// What type of network packet are we dealing with?
///
/// The associated number must be a u8
#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum PacketType {
    /// Unknown
    Unknown = 0,

    /// A preliminary Handshake before any further communications
    InitialHandshake = 1,

    /// A normal data packet
    Data = 2,

    /// A data packet that contains a file
    File = 3,

    /// A data packet that indicates a retrieval of a previously sent packet
    Retrieve = 4,

    /// A data packet that indicates a drop/delete of a previously sent packet
    Drop = 5,

    /// A data packet that contains some unformatted message
    Message = 6,

    /// A data packet that contains a request for and response list of all uploads still on the server for a secific access key
    ListUploads = 7,

    /// Repleace the file byte data of an already existing file upload
    Update = 8,

    /// Details of a previously transfered data packet
    PacketDetails = 10,

    /// An upgrade request of a client
    Upgrade = 11,

    /// A detailed reponse message, following an upload, to a client
    UploadResponse = 20,

    /// Indicates status of a process
    Status = 50,

    RsaPublicKey4096 = 100,

    /// Receiving a RSA4096 public key from the server
    RsaPublicKey2048 = 101,

    /// A generic key used for transfering any public key
    RsaPublicKeyGeneric = 102
}
impl PacketType {
    pub fn from_byte(byte: &u8) -> PacketType {
        match byte {
            1 => PacketType::InitialHandshake,
            2 => PacketType::Data,
            3 => PacketType::File,
            4 => PacketType::Retrieve,
            5 => PacketType::Drop,
            6 => PacketType::Message,
            7 => PacketType::ListUploads,
            8 => PacketType::Update,
            10 => PacketType::PacketDetails,
            11 => PacketType::Upgrade,
            20 => PacketType::UploadResponse,
            50 => PacketType::Status,
            100 => PacketType::RsaPublicKey4096,
            101 => PacketType::RsaPublicKey2048,
            102 => PacketType::RsaPublicKeyGeneric,
            _ => PacketType::Unknown,
        }
    }
}

/// Which encryption scheme should be applied to network packets
///
/// The associated number must be a u8
#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum EncryptionScheme {
    /// Unknown
    Unknown = 0,

    /// No Encryption
    None = 1,

    /// No RSA key exchange
    Aes128 = 2,

    /// No RSA key exchange
    Aes256 = 3,

    /// RSA2048 key handshaking
    Rsa2048Aes128 = 4,

    /// RSA2048 key handshaking
    Rsa2048Aes256 = 5,

    /// RSA4096 key handshaking
    Rsa4096Aes128 = 6,

    /// RSA4096 key handshaking
    Rsa4096Aes256 = 7,

}
impl EncryptionScheme {
    /// Convert a u8 into a named instance
    pub fn from_byte(byte: &u8) -> EncryptionScheme {
        match byte {
            1 => EncryptionScheme::None,
            2 => EncryptionScheme::Aes128,
            3 => EncryptionScheme::Aes256,
            4 => EncryptionScheme::Rsa2048Aes128,
            5 => EncryptionScheme::Rsa2048Aes256,
            6 => EncryptionScheme::Rsa4096Aes128,
            7 => EncryptionScheme::Rsa4096Aes256,
            _ => EncryptionScheme::Unknown,
        }
    }

    /// Returns true if we are doing any RSA type encryption
    pub fn use_public_or_private_key(encryption_scheme: &enums::EncryptionScheme) -> bool {
        match encryption_scheme {
            enums::EncryptionScheme::Rsa4096Aes256 
            | enums::EncryptionScheme::Rsa4096Aes128 
            | enums::EncryptionScheme::Rsa2048Aes256 
            | enums::EncryptionScheme::Rsa2048Aes128 => true,
            _ => false
        }
    }

    /// Encrypt bytes with AES256
    pub fn aes_256_encrypt(bytes_to_encrypt: &Vec<u8>, key: &Vec<u8>, salt: &Vec<u8>, encryption_scheme: EncryptionScheme, use_public_key: bool, rsa_key_pair_id: &u16, on_benchmarks_handler: fn(benchmarks: structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        crate::trace!(&crate::LABEL_ENCRYPTING);
        let result_encrypted = EncryptionScheme::encrypt_with_rsa_if_needed(&bytes_to_encrypt, &encryption_scheme, use_public_key, &rsa_key_pair_id);
        if result_encrypted.is_ok() {
            let option_encrypted = encryption::aes_encrypt_timed(enums::EncryptionScheme::Rsa4096Aes256, &result_encrypted.unwrap(), &salt, &key, on_benchmarks_handler);
            if option_encrypted.is_some(){
                Ok(option_encrypted.unwrap())
            } else {
                Err(String::from("Could not AES encrypt the data"))
            }
        } else {
            Err(String::from("Could not AES encrypt the data"))
        }
    }
    /// Decrypt bytes with AES256
    pub fn aes_256_decrypt(bytes_to_decrypt: &Vec<u8>, key: &Vec<u8>, encryption_scheme: &EncryptionScheme,  use_public_key: bool, on_benchmarks_handler: fn(structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        crate::trace!(&crate::LABEL_DECRYPTING);
        let mut option_decrypted = EncryptionScheme::decrypt_with_rsa_if_needed(&bytes_to_decrypt, &encryption_scheme, use_public_key);
        if option_decrypted.is_some() {
            let salt = bytes_to_decrypt[0..crate::LENGTH_OF_ENCRYPTION_SALT].to_vec();
            let bytes = option_decrypted.unwrap();
            let seedless_bytes_to_decrypt = bytes[crate::LENGTH_OF_ENCRYPTION_SALT..bytes.len()].to_vec();
            option_decrypted = encryption::aes_decrypt_timed(enums::EncryptionScheme::Rsa4096Aes256, &seedless_bytes_to_decrypt, &salt, &key, on_benchmarks_handler);
            if option_decrypted.is_some(){
                Ok(option_decrypted.unwrap())
            } else {
                Err(String::from("Could not AES decrypt the data"))
            }
        } else {
            Err(String::from("Could not RSA decrypt the data"))
        }
    }

    /// Encrypt bytes with AES128
    pub fn aes_128_encrypt(bytes_to_encrypt: &Vec<u8>, key: &Vec<u8>, salt: &Vec<u8>, encryption_scheme: EncryptionScheme, use_public_key: bool, rsa_key_pair_id: &u16, on_benchmarks_handler: fn(structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        crate::trace!(&crate::LABEL_ENCRYPTING);
        let result_encrypted = EncryptionScheme::encrypt_with_rsa_if_needed(&bytes_to_encrypt, &encryption_scheme, use_public_key, &rsa_key_pair_id);
        if result_encrypted.is_ok() {
            let option_encrypted = encryption::aes_encrypt_timed(enums::EncryptionScheme::Rsa4096Aes128, &result_encrypted.unwrap(), &salt, &key, on_benchmarks_handler);
            if option_encrypted.is_some(){
                Ok(option_encrypted.unwrap())
            } else {
                Err(String::from("Could not AES encrypt the data"))
            }
        } else {
            Err(String::from("Could not AES encrypt the data"))
        }
    }
    /// Decrypt bytes with AES128
    pub fn aes_128_decrypt(bytes_to_decrypt: &Vec<u8>, key: &Vec<u8>, encryption_scheme: &EncryptionScheme, use_public_key: bool, on_benchmarks_handler: fn(benchmarks: structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        crate::trace!(&crate::LABEL_DECRYPTING);
        let mut option_decrypted = EncryptionScheme::decrypt_with_rsa_if_needed(&bytes_to_decrypt, &encryption_scheme, use_public_key);
        if option_decrypted.is_some() {
            let salt = bytes_to_decrypt[0..crate::LENGTH_OF_ENCRYPTION_SALT].to_vec();
            let bytes = option_decrypted.unwrap();
            let seedless_bytes_to_decrypt = bytes[crate::LENGTH_OF_ENCRYPTION_SALT..bytes.len()].to_vec();
            option_decrypted = encryption::aes_decrypt_timed(enums::EncryptionScheme::Rsa4096Aes128, &seedless_bytes_to_decrypt, &salt,&key, on_benchmarks_handler);
            if option_decrypted.is_some(){
                Ok(option_decrypted.unwrap())
            } else {
                Err(String::from("Could not AES decrypt the data"))
            }
        } else {
            Err(String::from("Could not RSA decrypt the data"))
        }
    }

    /// If using an RSA encryption scheme then encrypt the bytes with the required algorythm
    pub fn encrypt_with_rsa_if_needed(bytes_to_encrypt: &Vec<u8>, encryption_scheme: &EncryptionScheme, use_public_key: bool, rsa_key_pair_id: &u16) -> Result<Vec<u8>, String> {
        if use_public_key {
            let encrypted_bytes = match encryption_scheme {
                EncryptionScheme::Rsa2048Aes256 | EncryptionScheme::Rsa2048Aes128 => {
                    let rsa_key_pair = &crate::RSA_KEY_PAIRS_CLIENT.get_key_pair(&rsa_key_pair_id)?.rsa_2048_key_pair;
                    let rsa_public_key = encryption::rsa_get_public_key(&rsa_key_pair.public_key);
                    
                    encryption::rsa_encrypt_public(&rsa_public_key, &bytes_to_encrypt)
                },
                EncryptionScheme::Rsa4096Aes256 | EncryptionScheme::Rsa4096Aes128 => {
                    let rsa_key_pair = &crate::RSA_KEY_PAIRS_CLIENT.get_key_pair(&rsa_key_pair_id)?.rsa_4096_key_pair;
                    let rsa_public_key = encryption::rsa_get_public_key(&rsa_key_pair.public_key);
                    
                    let option = encryption::rsa_encrypt_public(&rsa_public_key, &bytes_to_encrypt);
                    
                    option
                }
                _ => Some(bytes_to_encrypt.clone())
            };
            if encrypted_bytes.is_some() {
                
                Ok(encrypted_bytes.unwrap())
            } else {
                
                Err("Could not perform RSA encryption".into())
            }
        } else {
            Ok(bytes_to_encrypt.clone())
        }
    }

    /// Use the public RSA key of a client to encrypt bytes_to_encrypt
    pub fn encrypt_with_client_public_rsa_key(bytes_to_encrypt: &Vec<u8>, p2p_client_name: &str) -> Result<Option<Vec<u8>>, String> {
        let result_encrypted_bytes = {
            let option_rsa_key_pair = &crate::RSA_KEY_PAIRS_CLIENT_PRIVATE.public_rsa_4096_keys.iter().find(|k| &k.name==p2p_client_name);
            if option_rsa_key_pair.is_some() {
                let rsa_public_key = encryption::rsa_get_public_key(&option_rsa_key_pair.unwrap().public_key);
                Ok(encryption::rsa_encrypt_public(&rsa_public_key, &bytes_to_encrypt))
            } else {
                Err(format!("Could not perform RSA encryption with public key for client named {}", p2p_client_name))
            }
        };

        result_encrypted_bytes
    }

    /// Use the public RSA key of the client to encrypt bytes_to_encrypt
    pub fn encrypt_with_own_client_public_rsa_key(bytes_to_encrypt: &Vec<u8>) -> Result<Option<Vec<u8>>, String> {
        let result_encrypted_bytes = {
            let public_rsa_key_bytes = &crate::RSA_KEY_PAIRS_CLIENT_PRIVATE.client_rsa_4096_key_pair.public_key;
            let rsa_public_key = encryption::rsa_get_public_key(&public_rsa_key_bytes);
            Ok(encryption::rsa_encrypt_public(&rsa_public_key, &bytes_to_encrypt))
        };

        result_encrypted_bytes
    }

    /// Use the private RSA key of the client to decrypt bytes_to_decrypt
    pub fn decrypt_with_client_private_rsa_key(bytes_to_decrypt: &Vec<u8>) -> Result<Option<Vec<u8>>, String> {
        let rsa_key_bytes = &crate::RSA_KEY_PAIRS_CLIENT_PRIVATE.client_rsa_4096_key_pair.private_key;
        let rsa_private_key = encryption::rsa_get_private_key(&rsa_key_bytes);
        let option_decrypted_bytes = encryption::rsa_decrypt_private(&rsa_private_key, bytes_to_decrypt);
        if option_decrypted_bytes.is_some() {
            Ok(option_decrypted_bytes)
        } else {
            Err("Could not decrypt the bytes using the clients' private RSA key".into())
        }
    }

    /// If using an RSA decryption scheme then decrypt the bytes with the required algorythm
    pub fn decrypt_with_rsa_if_needed(bytes_to_decrypt: &Vec<u8>, encryption_scheme: &EncryptionScheme, use_private_key: bool) -> Option<Vec<u8>> {
        if use_private_key {
            let decrypted_bytes = match encryption_scheme {
                EncryptionScheme::Rsa2048Aes256 | EncryptionScheme::Rsa2048Aes128 => {
                    let rsa_key_pair = &crate::RSA_KEY_PAIRS_SERVER.rsa_2048_key_pair;
                    let rsa_private_key = encryption::rsa_get_private_key(&rsa_key_pair.private_key);
                    encryption::rsa_decrypt_private(&rsa_private_key, &bytes_to_decrypt)
                },
                EncryptionScheme::Rsa4096Aes256 | EncryptionScheme::Rsa4096Aes128 => {
                    let rsa_key_pair = &crate::RSA_KEY_PAIRS_SERVER.rsa_4096_key_pair;
                    let rsa_private_key = encryption::rsa_get_private_key(&rsa_key_pair.private_key);
                    
                    let p = encryption::rsa_decrypt_private(&rsa_private_key, &bytes_to_decrypt);
                    
                    p
                },
                _ => Some(bytes_to_decrypt.clone())
            };
            
            decrypted_bytes
        } else {
            Some(bytes_to_decrypt.clone())
        }
    }
}

/// How sould encryption and compression be applied?
///
/// The associated number must be a u8
#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum EncryptionPacketSheme {
    /// None
    None = 0,

    /// Encrypt/Decrypt each packet seperately
    PerPacket = 1,

    /// Encrypt the entire stream at once before sending and
    /// decrypt the entire stream on receiving
    PerSession = 2,

    /// Unknown
    Unknown = 255,
}
impl EncryptionPacketSheme {
    /// Convert a u8 into a named instance
    pub fn from_byte(byte: u8) -> EncryptionPacketSheme {
        match byte {
            0 => EncryptionPacketSheme::None,
            1 => EncryptionPacketSheme::PerPacket,
            2 => EncryptionPacketSheme::PerSession,
            _ => EncryptionPacketSheme::Unknown,
        }
    }
}

/// What type of error occured
///
/// The associated number must be a u8
#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum Status {
    Unknown = 0,
    Ok = 1,
    Error = 10
}
impl Status {
    /// Convert a u8 into a named instance
    pub fn from_byte(byte: u8) -> Status {
        match byte {
            1 => Status::Ok,
            2 => Status::Error,
            _ => Status::Unknown,
        }
    }
}

#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum BenchmarkType {
    Encryption = 0,
    Decryption = 1,
    Compression = 2,
    Decompression = 3
}

/// Must the data be compressed before being sent?
///
/// The associated number must be a u8
#[derive(Copy, Clone, Serialize_repr, Deserialize_repr, PartialEq, Debug)]
#[repr(u8)]
pub enum EnableCompression {
    /// Unknown
    Unknown = 0,

    ///Dont compress
    No = 1,

    /// Compress
    Yes = 2,
}
impl EnableCompression {
    /// Convert a u8 into a named instance
    pub fn from_byte(byte: &u8) -> EnableCompression {
        match byte {
            1 => EnableCompression::No,
            2 => EnableCompression::Yes,
            _ => EnableCompression::Unknown,
        }
    }

    /// Compress bytes
    pub fn compress_bytes(bytes_to_compress: &Vec<u8>, on_benchmarks_handler: fn(benchmarks: structs::BenchmarkResult)) -> Result<Vec<u8>, String> {

        let option_kill_sender = structs::Benchmarks::show_predicted_progress(&crate::LABEL_COMPRESSING, bytes_to_compress.len() as f32, crate::BENCHMARKS.lock().unwrap().compression_bytes_per_second);
        let start = std::time::Instant::now();

        let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
        let _ = encoder.write_all(bytes_to_compress);
        let result_compressed_bytes = encoder.finish();

        if option_kill_sender.is_some() {
            let _ = option_kill_sender.unwrap().send(true);
        };

        if result_compressed_bytes.is_ok() {
            let result = structs::BenchmarkResult::new(enums::BenchmarkType::Compression, 0, bytes_to_compress.len(), start.elapsed());
            on_benchmarks_handler(result);
            Ok(result_compressed_bytes.unwrap())
        } else {
            Err(String::from("Compression error"))
        }
    }

    /// Decompress bytes
    pub fn decompress_bytes(bytes_to_decompress: &Vec<u8>, on_benchmarks_handler: fn(benchmarks: structs::BenchmarkResult)) -> Result<Vec<u8>, String> {
        let option_kill_sender = structs::Benchmarks::show_predicted_progress(&crate::LABEL_DECOMPRESSING, bytes_to_decompress.len() as f32, crate::BENCHMARKS.lock().unwrap().decompression_bytes_per_second);
        let start = std::time::Instant::now();

        let decompressed_bytes = Vec::new();
        let mut decoder = ZlibDecoder::new(decompressed_bytes);
        let _ = decoder.write_all(&bytes_to_decompress[..]).unwrap();
        let result_decompressed_bytes = decoder.finish();

        if option_kill_sender.is_some() {
            let _ = option_kill_sender.unwrap().send(true);
        };

        if result_decompressed_bytes.is_ok() {
            let result = structs::BenchmarkResult::new(enums::BenchmarkType::Decompression, 0, bytes_to_decompress.len(), start.elapsed());

            on_benchmarks_handler(result);

            Ok(result_decompressed_bytes.unwrap())
        } else {
            Err(String::from("Decompression error"))
        }
    }
}

/// Indicates the type of application this library is compiled with.
#[derive(PartialEq)]
pub enum Application {
    Server,
    Client
  }