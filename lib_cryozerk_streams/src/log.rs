/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Log files are written in this module.
//!
//! The macro's used system wide to diplay information to the user and write to log files are also kept here

use std::sync::Mutex;
use super::general;
use super::files;

/// Possibly display information to the user but always write it to the log file in the working directory
#[macro_export]
macro_rules! log {
    ($e:expr) => {
        crate::log::write_log(true, $e, false);
    };
    ($a:expr, $b:expr) => {
        crate::log::write_log_with_details(true, &format!("{}", $a), &format!("{:?}", $b), false);
    };
}

/// Display information to the user irrespective of any other overriding setting and always write it to the log file in the working directory too
#[macro_export]
macro_rules! display {
    ($e:expr) => {
        crate::log::write_log(true, $e, true);
    };
    ($a:expr, $b:expr) => {
        crate::log::write_log_with_details(true, &format!("{}", $a), &format!("{:?}", $b), true);
    };
}

/// Information to only be written to the log file and not shown to the user
#[macro_export]
macro_rules! trace {
    ($e:expr) => {
        crate::log::write_log(false, $e, false);
    };
    ($a:expr, $b:expr) => {
        //if cfg!(debug_assertions) {
        crate::log::write_log_with_details(false, &format!("{}", $a), &format!("{:?}", $b), false);
        //}
    };
}

/// Write to the log file as the calling macros indicate
pub fn write_log(display_on_console: bool, message: &str, override_quiet_mode: bool) {
    if unsafe{crate::DARK_MODE==false}{
        if unsafe{crate::ENABLE_LOGGING}
        {
            if cfg!(windows) {
                println!("{}", message);
            } else {
                unsafe {
                    if override_quiet_mode || (crate::QUIET_MODE==0 && display_on_console) {
                        println!("{}", message);
                    }
                }
            }

            let log_entry = format!("-{:?}: {}", general::get_current_datestamp(), message);
            let mut log_entry_vec = log_entry.as_bytes().to_vec();
            log_entry_vec.push(13);
            log_entry_vec.push(10);
            
            let result_path = files::local_file_exists(&crate::LOG_FILE_NAME); // ! Fix this quick fix
            let path: String;
            if result_path.is_ok() {
                path = result_path.unwrap();
            } else {
                path = result_path.unwrap_err();
            }

            let guard = Mutex::new(0_u32);
            let lock = guard.lock();
            if !files::write_to_file(&path, &log_entry_vec, true){
                println!("ERROR WRITING TO LOG: {}", log_entry);
            }

            rollover_log_file(&path);

            drop(lock);
        }
    }
}

/// Write to the log file as the calling macros indicate
pub fn write_log_with_details(display_on_console: bool, message: &str, details: &str, override_quiet_mode: bool) {
    if unsafe{crate::DARK_MODE==false}{
        if unsafe{crate::ENABLE_LOGGING}
        { 
            if cfg!(windows) {
                println!("{}: {}", message, details);
            } else {
                unsafe {
                    if override_quiet_mode || (crate::QUIET_MODE==0 && display_on_console) {
                        println!("{}: {}", message, details);
                    }
                }
            }

            let log_entry = format!("-{:?}: {}: {}", general::get_current_datestamp(), message, details);
            let mut log_entry_vec = log_entry.as_bytes().to_vec();
            log_entry_vec.push(13);
            log_entry_vec.push(10);

            let result_path = files::local_file_exists(&crate::LOG_FILE_NAME);
            let path: String;
            if result_path.is_ok() {
                path = result_path.unwrap();
            } else {
                path = result_path.unwrap_err();
            }

            let guard = Mutex::new(0_u32);
            let lock = guard.lock();
            if !files::write_to_file(&path, &log_entry_vec, true){
                println!("ERROR WRITING TO LOG: {}", log_entry);
            }

            rollover_log_file(&path);

            drop(lock);
        }
    }
}

fn rollover_log_file(path: &str){
    let file_handle = std::fs::File::open(&path).unwrap();
    if file_handle.metadata().unwrap().len() > 1024*1024*10 {
        drop(file_handle);
        let new_path = &files::get_path_to_local_file(&format!("{}-{}", crate::general::get_current_datestamp().format("%Y-%m-%d-%H:%M:%S"), *crate::LOG_FILE_NAME));
        let result = std::fs::rename(&path, &new_path.to_str().unwrap());
        if result.is_ok() {
            println!("Log file roll-over done");
        } else {
            println!("Could not roll-over log file '{}': {}", &path, result.unwrap_err());
        }
    }
}
