/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! General unspecific functions that may be called from anywhere in the system

/// General methods
use chrono::{DateTime, Local};
use crate::log;

use super::structs;
use super::general;
use super::enums;

/// Get the current local time., this where the format used by the DateTime stamps are generated from
pub fn get_current_datestamp() -> DateTime<Local> {
    Local::now()
}

/// Append 0 to end of vector to pad to specified length
pub fn append_vector_to_length(vector: Vec<u8>, new_length: usize) -> Vec<u8> {
    let mut new_vector = vector;
    if new_vector.len() < new_length {
        for _ in 0..(new_length - new_vector.len()) {
            new_vector.push(0);
        }
    }

    new_vector
}

/// Get a specified number of random characters from a given list(character_set)
pub fn get_random_characters_from(character_set: Vec<&str>, character_count: u16) -> String {
    let mut selected_characters: Vec<&str> = vec![];
    for _ in 0..character_count {
        selected_characters.push(character_set[get_a_random_number_in_range(0, (character_set.len() - 1) as u64) as usize]);
    }

    selected_characters.into_iter().collect()
}

///Get a random word
pub fn get_random_word() -> String {
    let character_set: Vec<&str> = vec! [
        "dog", "cat", "lion", "tiger", "wolf", "hound", "hamster", "mouse", "ape", "monkey",
        "cow", "chicken", "pig", "bull", "rooster", "duck", "goose", "geese", "horse", "goat",
        "shark", "whale", "mantaray", "dugong", "zambezi", "molly", "tetra", "siamese", "blowfish", "urchin",
        "this", "that", "me", "you", "up", "down", "left", "right", "in", "out",
        "red", "orange", "yellow", "green", "blue", "indigo", "violet", "black", "silver", "gold",
        "John", "Sally", "Peter", "Steve", "Mark", "Jenny", "Beth", "Maria", "Frank", "Lizzy",
        "George", "Marge", "Dean", "Sally", "Thomas", "Robby", "Elizabeth", "Mathew", "Sean", "Paddy",
        "everest", "dune", "high", "mountain", "hill", "mound", "cliff", "tall", "heights", "solid",
        "crypto", "bastion", "deep", "ocean", "water", "sand", "pebble", "rock", "sand", "range",
        "fun", "laughter", "sing", "play", "tickle", "giggle", "jest", "toy", "blabber", "smile",
        "crypto", "hide", "gone", "cypher", "crypted", "where", "lost", "strong", "vanish", "secure",
        "upsidedown", "rightwayup", "totheleft", "totheright", "notthisway", "notthatway", "ididitmyway", "turnaround", "leftfootin", "rightfootin", 
        "hexagon", "octagon", "pentagon", "square", "circle", "triangle", "ellipse", "3d", "4d", "5d",
        "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eight", "ninth", "tenth",
        "USA", "greenland", "australia", "russia", "southkorea", "iceland", "japan", "england", "ireland", "scotland",
        "tough", "strong", "power", "titan", "obelisk", "mastadon", "iceberg", "solid", "tight", "bearhug", 
        "catcher", "thrower", "passer", "wingman", "eigthman", "bowler", "rightwing", "fullback", "putter", "ref",
        "12345", "54321", "123", "321", "111", "222", "11111", "22222", "33333", "55555", 
        "lowercase", "atoA", "atoz", "atozAtoZ", "0to9ANDatoz", "0to9AtoZ", "XANDY", "specialchars", "alphanumeric", "UPPERCASE", 
        "gonewiththewind", "lordoftheflies", "tomsawyer", "hardyboys", "annefrank", "awinterstale", "dieHard", "FarAndAway", "Titanic", "kingOFqueens"
    ];
    get_random_characters_from(character_set, 1)
}

///Get a string of random alphanumeric characters
pub fn get_random_alphanumeric_characters(character_count: u16) -> String {
    let character_set: Vec<&str> = vec! [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1",
        "2", "3", "4", "5", "6", "7", "8", "9",
    ];
    get_random_characters_from(character_set, character_count)
}

///Get a string of random alphanumeric characters
pub fn get_random_alphabetic_characters(character_count: u16) -> String {
    let character_set: Vec<&str> = vec! [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
        "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    ];
    get_random_characters_from(character_set, character_count)
}

///Get a random number within a specified number range
pub fn get_a_random_number_in_range(start_value: u64, end_value: u64) -> u64 {
    use rand::Rng;
    let mut rng = rand::thread_rng();

    rng.gen_range(start_value, end_value)
}

/// Convert the bytes to a u64 number
pub fn get_64bit_number_from(bytes: &Vec<u8>) -> u64 {
    if bytes.len() != 0 {
        let mut number_bytes: [u8; crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize] = [0; crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize];
        for i in 0..number_bytes.len() {
            number_bytes[i] = bytes[i];
        }

        u64::from_be_bytes(number_bytes)
    } else {
        log!("There may be a problem in deserialising a 64bit number byte array");
        0
    }
}

/// Convert the bytes to a u32 number
pub fn get_32bit_number_from(bytes: &Vec<u8>) -> u32 {
    if bytes.len() != 0 {
        let mut number_bytes: [u8; crate::LENGTH_OF_U32_NUMBER_IN_BYTES as usize] = [0; crate::LENGTH_OF_U32_NUMBER_IN_BYTES as usize];
        for i in 0..number_bytes.len() {
            number_bytes[i] = bytes[i];
        }

        u32::from_be_bytes(number_bytes)
    } else {
        log!("There may be a problem in deserialising a 32bit number byte array");
        0
    }
}

/// Convert the bytes to a u16 number
pub fn get_16bit_number_from(bytes: &Vec<u8>) -> u16 {
    if bytes.len() != 0 {
        let mut number_bytes: [u8; 2] = [0; 2];
        for i in 0..number_bytes.len() {
            number_bytes[i] = bytes[i];
        }

        u16::from_be_bytes(number_bytes)
    } else {
        log!("There may be a problem in deserialising a 16bit number byte array");
        0
    }
}

/// Get the version tuple so as to be enable to compare versions among installations
pub fn get_version_tuple() -> (u8, u8, u8) {
    let splitter = crate::VERSION_INFO.rsplit_terminator('.');
    let mut iter = splitter.enumerate();
    let splitter_major = iter.next().unwrap().1;
    let splitter_minor = iter.next().unwrap().1;
    let splitter_revision = iter.next().unwrap().1;
    let revision =  u8::from_str_radix(&splitter_major, 10).unwrap();
    let minor: u8 = u8::from_str_radix(&splitter_minor, 10).unwrap();
    let major: u8 = u8::from_str_radix(&splitter_revision, 10).unwrap();

    (major, minor, revision)
}

/// Get the symmetric key used to encrypt data packages.
///
/// It may aleady be also encrypted with a RSA key so check that too
pub fn get_symmetric_encryption_key_for_session(initial_handshake: &structs::InitialHandshake) ->Vec<u8> {
    let new_key = match initial_handshake.encryption_scheme {
        enums::EncryptionScheme::Rsa4096Aes256 | enums::EncryptionScheme::Rsa4096Aes128 => {
            let rsa_private_key = crate::encryption::rsa_get_private_key(&crate::RSA_KEY_PAIRS_SERVER.rsa_4096_key_pair.private_key);
            crate::encryption::rsa_decrypt_private(&rsa_private_key,&(*initial_handshake.packet_symmetric_encryption_key).to_vec()).unwrap()
        }
        enums::EncryptionScheme::Rsa2048Aes256 | enums::EncryptionScheme::Rsa2048Aes128 => {
            let rsa_private_key = crate::encryption::rsa_get_private_key(&crate::RSA_KEY_PAIRS_SERVER.rsa_2048_key_pair.private_key);
            let option = crate::encryption::rsa_decrypt_private(&rsa_private_key, &(*initial_handshake.packet_symmetric_encryption_key).to_vec());
            if option.is_some(){
                option.unwrap()
            } else {
                crate::log!("Could not decrypt private key", initial_handshake.packet_symmetric_encryption_key);
                std::process::exit(1);
            }
        }
        _ => initial_handshake.packet_symmetric_encryption_key.clone(),
    };

    crate::trace!("Session key", &new_key);

    new_key
}

/// Remove all the number 0 bytes from the END of a vector
pub fn strip_zeros(vector: &Vec<u8>) -> Vec<u8> {
    let mut tmp_vector = vector.to_owned();

    tmp_vector.reverse();
    let mut return_vector: Vec<u8> = vec![];
    
    let mut found_first_char: bool = false;
    for i in 0..tmp_vector.len() {
        if tmp_vector[i] != 0 {
            return_vector.push(tmp_vector[i]);
            found_first_char = true;
        } else {
            if found_first_char {
                return_vector.push(tmp_vector[i]);
            }
        }
    }
    return_vector.reverse();

    return_vector
}

/// Convert a vector ending in 0's to a String stripped from the ending 0's
pub fn vector_stripped_to_string(vector: &Vec<u8>) -> String {
    let stripped_vector = &general::strip_zeros(&vector);

    std::str::from_utf8(stripped_vector).unwrap().into()
}

pub fn calculate_padding_length(data_length: usize) -> u32 {
    let padding: f32 = if data_length <= 1024*1024*2 {
        data_length as f32 * ((get_a_random_number_in_range(5, 90) as f32 / 100.0))
    } else if data_length <= 1024*1024*75 {
        data_length as f32 * ((get_a_random_number_in_range(2, 30) as f32 / 100.0))
    } else {
        data_length as f32 * ((get_a_random_number_in_range(2, 15) as f32 / 100.0))
    };
    
    padding as u32
}

/// Returns a more human readable format of the bytes count supplied
pub fn format_count_to_string(length_of_bytes: usize) -> String {
    if length_of_bytes > 1024*1024*1024*1024 { // > 1TiB
        format!("{:.3}TiB", length_of_bytes as f64 / (1024_f64*1024_f64*1024_f64*1024_f64))
    } else if length_of_bytes > 1024*1024*1024 { // > 1GiB
        format!("{:.3}GiB", length_of_bytes as f64 / (1024_f64*1024_f64*1024_f64))
    } else if length_of_bytes > 1024*1024 { // > 1MiB
        format!("{:.3}MiB", length_of_bytes as f64 / (1024_f64*1024_f64))
    } else if length_of_bytes > 1024*5 { // > 5KiB
        format!("{:.3}KiB", length_of_bytes as f64 / 1024_f64)
    } else {
        format!("{}bytes", length_of_bytes)
    }
}