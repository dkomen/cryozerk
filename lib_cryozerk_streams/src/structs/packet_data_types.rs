/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! More specialised Data packets.
//! Most packets sent or received will be a DataPacket that wraps one of these more specialised packets
use std::vec;

use chrono::{DateTime, Local};
use super::enums;
use super::log;
use super::structs::serialisation::{Serialise, Deserialise};
use super::general;
/// Used to send a file
#[derive(Debug)]
pub struct File {
    pub length: u64,
    pub padding_length: u32,
    pub padding_bytes: Vec<u8>,
    pub verification_key: Vec<u8>,
    pub unique_code: Vec<u8>,
    /// A key used to verify a second password before allowing actions like deletions. If all values are 0 then file is not protected.
    pub protected_key: Vec<u8>,
    pub packet_type: super::enums::PacketType,
    pub is_compressed: enums::EnableCompression, 
    pub file_name: Vec<u8>,
    pub description: String,
    pub layer: String,
    pub drop_on_retrieval: bool,
    pub bytes: Vec<u8>,
    pub is_p2p_encrypted: bool,
    pub p2p_secret: Vec<u8>,
    /// A hash of the unencrypted and uncompressed file bytes
    pub hash: Vec<u8>
}
impl File {
    /// Create a new instance of File
    pub fn new(bytes: &Vec<u8>, file_name: &str, layer: &str, description: &str, drop_on_retrieval: bool, verification_key_padded: &Vec<u8>, unique_code_padded: &Vec<u8>, protected_key_padded: &Vec<u8>, is_p2p_encrypted: bool) -> File {
        let padding_length = general::calculate_padding_length(bytes.len());
        let hash =crate::encryption::generate_sha_512_hash(bytes);
        
        File {
            length: 0,
            packet_type: enums::PacketType::File,
            padding_length: padding_length,
            padding_bytes: crate::encryption::generate_random_vector(padding_length),
            verification_key: verification_key_padded.to_owned(),
            unique_code: unique_code_padded.to_owned(),
            protected_key: protected_key_padded.to_owned(),
            file_name: file_name.as_bytes().to_vec(),
            is_compressed: enums::EnableCompression::Yes,
            drop_on_retrieval: drop_on_retrieval,
            description: description.into(),
            layer: layer.into(),
            bytes: bytes.to_vec(),
            is_p2p_encrypted: is_p2p_encrypted,
            p2p_secret: crate::encryption::generate_random_vector(crate::LENGTH_OF_P2P_SECRET as u32),
            hash: hash
        }
    }

    /// Serialise into bytes
    pub fn into_bytes(&mut self) -> Result<Vec<u8>, String> {
        let mut description_to_use = self.description.as_bytes().to_vec();
        let mut layer_to_use = self.layer.as_bytes().to_vec();

        // Validate file name
        if self.file_name.len() > crate::MAXIMUM_LENGTH_FILE_NAME {
            let error_message = format!("Length of file name exceeds maximum of {}", crate::MAXIMUM_LENGTH_FILE_NAME);
            log!(&error_message);
            return Err(error_message);
        } 

        // Validate description
        if self.description.len() > crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD {
            let error_message = format!("Length of description exceeds maximum of {}", crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD);
            log!(&error_message);
            return Err(error_message);
        }

        // Validate layer
        if self.layer.len() > crate::MAXIMUM_LENGTH_LAYER {
            let error_message = format!("Length of layer exceeds maximum of {}", crate::MAXIMUM_LENGTH_LAYER);
            log!(&error_message);
            return Err(error_message);
        }

        let mut serialiser = Serialise::new("File");
        //let bytes_length = self.bytes.len() + crate::LENGTH_OF_U32_NUMBER_IN_BYTES + self.padding_length as usize;
        let bytes_length = self.bytes.len();

        serialiser.add_field(&mut vec![self.packet_type as u8], 1)?;
        serialiser.add_field(&mut self.padding_length.to_owned().to_be_bytes().to_vec(), crate::LENGTH_OF_U32_NUMBER_IN_BYTES as usize)?;
        serialiser.add_field(&mut self.padding_bytes, self.padding_length as usize)?;
        serialiser.add_field(&mut self.verification_key.to_owned(), crate::MAXIMUM_LENGTH_VERIFICATION_KEY as usize)?;
        serialiser.add_field(&mut self.unique_code.to_owned(), crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE as usize)?;
        serialiser.add_field(&mut self.protected_key.to_owned(), crate::MAXIMUM_LENGTH_VERIFICATION_KEY as usize)?;
        serialiser.add_field(&mut vec![self.is_compressed as u8], 1)?;
        serialiser.add_field(&mut vec![self.drop_on_retrieval as u8], 1)?;
        serialiser.add_field(&mut self.file_name, crate::MAXIMUM_LENGTH_FILE_NAME)?;
        serialiser.add_field(&mut layer_to_use, crate::MAXIMUM_LENGTH_LAYER)?;
        serialiser.add_field(&mut description_to_use, crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?;
        serialiser.add_field(&mut vec![self.is_p2p_encrypted as u8], 1)?;
        serialiser.add_field(&mut self.p2p_secret, crate::MAXIMUM_LENGTH_RSA_4096_P2P_SECRET)?;
        serialiser.add_field(&mut self.hash, 64)?;
        serialiser.add_field(&mut self.bytes, bytes_length)?;

        Ok(serialiser.into_bytes())
    }

    /// Deserialise from bytes
    pub fn from_bytes(bytes: Vec<u8>) -> Result<File, String> {
        let mut deserialiser = Deserialise::new(&bytes, "File");
        let length = general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?);
        let packet_type = super::enums::PacketType::from_byte(&(deserialiser.extract_field(1)?[0] as u8));
        let padding_length = general::get_32bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U32_NUMBER_IN_BYTES)?);
        
        let file = File {
            length: length,
            packet_type: packet_type,
            padding_length: padding_length,
            padding_bytes: deserialiser.extract_field(padding_length as usize)?,
            verification_key: deserialiser.extract_field(crate::MAXIMUM_LENGTH_VERIFICATION_KEY)?,
            unique_code: deserialiser.extract_field(crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)?,
            protected_key: deserialiser.extract_field(crate::MAXIMUM_LENGTH_VERIFICATION_KEY)?,
            is_compressed: super::enums::EnableCompression::from_byte(&(deserialiser.extract_field(1)?[0] as u8)),
            drop_on_retrieval: deserialiser.extract_field(1)?[0] as u8 == 1,
            file_name: deserialiser.extract_field(crate::MAXIMUM_LENGTH_FILE_NAME)?,
            layer: File::convert_to_string(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_LAYER)?),
            description: File::convert_to_string(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?),
            is_p2p_encrypted: deserialiser.extract_field(1)?[0] as u8 == 1,
            p2p_secret: deserialiser.extract_field(crate::MAXIMUM_LENGTH_RSA_4096_P2P_SECRET)?,
            hash: deserialiser.extract_field(64)?,

            bytes: deserialiser.extract_field_to_end()?
        };

        Ok(file)
    }

    /// Extract a field out of the bytes
    fn convert_to_string(bytes: &Vec<u8>) -> String {
        general::vector_stripped_to_string(&bytes)
    }
}

/// Holds information about a previous data packet
#[derive(Debug)]
pub struct PacketDetails {
    pub length: u64,
    pub packet_type: super::enums::PacketType,
    pub is_compressed: enums::EnableCompression,
}
impl PacketDetails {
    /// Create a new message with the supplied message data
    pub fn new(data_length: u64, is_compressed: enums::EnableCompression) -> PacketDetails {
        PacketDetails {
            length: data_length,
            packet_type: enums::PacketType::PacketDetails,
            is_compressed: is_compressed
        }
    }
    /// Serialise into bytes
    pub fn into_bytes(&self) -> Result<Vec<u8>, String> {
        let mut new_vector: Vec<u8> = vec![];

        // Length must be first in Vector
        let mut length: Vec<u8> = (crate::LENGTH_OF_DATA_PACKET_HEADER as u64 + 2).to_be_bytes().to_vec();
        new_vector.append(&mut length);
        new_vector.push(self.packet_type as u8);
        new_vector.push(self.is_compressed as u8);

        Ok(new_vector)
    }

    /// Deserialise from bytes
    pub fn from_bytes(bytes: Vec<u8>) -> Result<PacketDetails, String> {
        Ok(PacketDetails {
            length: bytes.len() as u64,
            packet_type: super::enums::PacketType::from_byte(&bytes[crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize]),
            is_compressed: super::enums::EnableCompression::from_byte(&bytes[crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize + 1]),
        })
    }
}
/// Used to send/receive a simple array of bytes for generic use
#[derive(Debug)]
pub struct Message {
    pub length: u64,
    pub packet_type: super::enums::PacketType,
    pub bytes: Vec<u8>,
}
impl Message {
    /// Create a new message with the supplied message data
    pub fn new(packet_type: enums::PacketType, message_data: &Vec<u8>) -> Message {
        Message {
            length: 0,
            packet_type: packet_type,
            bytes: message_data.to_owned()
        }
    }

    /// Serialise into bytes
    pub fn into_bytes(&self) -> Result<Vec<u8>, String> {
        let mut new_vector: Vec<u8> = vec![];

        // Length must be first in Vector
        let mut length: Vec<u8> = (crate::LENGTH_OF_DATA_PACKET_HEADER as u64 + self.bytes.len() as u64).to_be_bytes().to_vec();
        new_vector.append(&mut length);
        new_vector.push(self.packet_type as u8);
        new_vector.append(&mut self.bytes.to_owned());

        Ok(new_vector)
    }

    /// Deserialise from bytes
    pub fn from_bytes(bytes: Vec<u8>) -> Result<Message, String> {
        let length = general::get_64bit_number_from(&bytes);
        let message = Message {
            length: length,
            packet_type: super::enums::PacketType::from_byte(&bytes[crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize]),
            bytes: bytes[crate::LENGTH_OF_DATA_PACKET_HEADER as usize..].to_vec(),
        };

        Ok(message)
    }
}

/// A response message to return to the client after a 'on_file_arrived' was handled
#[derive(Clone, Debug)]
pub struct UploadResponse {
    length: u64,
    pub unique_code: Vec<u8>,
    pub user_message: Vec<u8>,
    pub will_be_deleted_on_retrieve: bool,
    pub status_code: enums::Status,
} 
impl UploadResponse {
    pub fn new(status_code: enums::Status, unique_code: &Vec<u8>, user_message: &Vec<u8>, will_be_deleted_on_retrieve: bool) -> UploadResponse {

        UploadResponse {
            length: 0,
            status_code: status_code,
            unique_code: unique_code.to_owned(),
            user_message: user_message.to_owned(),
            will_be_deleted_on_retrieve: will_be_deleted_on_retrieve
        }
    }

    // A new empty/un-initialised response
    pub fn new_empty() -> UploadResponse {

        UploadResponse {
            length: 0,
            status_code: enums::Status::Ok,
            unique_code: vec![],
            user_message: vec![],
            will_be_deleted_on_retrieve: true
        }
    }

    /// Serialise into bytes
    pub fn into_bytes(&mut self) -> Result<Vec<u8>, String> {
        let mut serialise = Serialise::new("UploadResponse");

        serialise.add_field(&mut self.unique_code, crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)?;
        serialise.add_field(&mut self.user_message, crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?;
        serialise.add_field(&mut vec![self.will_be_deleted_on_retrieve as u8], 1)?;
        serialise.add_field(&mut vec![self.status_code as u8], 1)?;

        Ok(serialise.into_bytes())
    }

    /// Serialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>) -> Result<UploadResponse, String> {
        let mut deserialiser = Deserialise::new(&bytes, "UploadResponse");

        let upload_esponse = UploadResponse {
            length: general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?),
            unique_code: general::strip_zeros(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)?),
            user_message: general::strip_zeros(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?),
            will_be_deleted_on_retrieve: deserialiser.extract_field(1)?[0] as u8 == 1,
            status_code: enums::Status::from_byte(deserialiser.extract_field(1)?[0] as u8)
        };

        Ok(upload_esponse)
    }
}
#[derive(Clone, Debug)]
pub struct ListItem {
    length: u64,
    pub unique_code: Vec<u8>,
    pub protected: bool,
    pub layer: Vec<u8>,
    pub description: Vec<u8>,
    pub drop_on_retrieval: bool,
    pub creation_time_stamp: Vec<u8>,
    pub auto_deletion_time_stamp: Vec<u8>,
    pub downloads_count: u64,
    pub bytes_length: u64
} 
impl ListItem {
    pub fn new(unique_code: Vec<u8>, protected: bool, layer: Vec<u8>, description: Vec<u8>, drop_on_retrieval: bool, downloads_count: u64, creation_time_stamp: DateTime<Local>, timeout_seconds: u64, bytes_length: u64) -> ListItem {
        let c_date_stamp = general::append_vector_to_length(format!("{}", creation_time_stamp).as_bytes().to_vec(), crate::LENGTH_OF_DATESTAMP_FIELD as usize);
        let duration = chrono::Duration::seconds(timeout_seconds as i64);
        let deletion_date_stamp= creation_time_stamp.checked_add_signed(duration).unwrap();
        let mut d_date_stamp = general::append_vector_to_length(format!("{}", deletion_date_stamp).as_bytes().to_vec(), crate::LENGTH_OF_DATESTAMP_FIELD as usize);

        if timeout_seconds==0 {
            d_date_stamp = general::append_vector_to_length(format!("Never").as_bytes().to_vec(), crate::LENGTH_OF_DATESTAMP_FIELD as usize);
        }

        let list_item = ListItem {
            length: 0,
            unique_code: unique_code,
            protected: protected,
            layer: layer,
            description: description,
            drop_on_retrieval: drop_on_retrieval,
            creation_time_stamp: c_date_stamp,
            auto_deletion_time_stamp: d_date_stamp,
            downloads_count: downloads_count,
            bytes_length: bytes_length
        };

        list_item
    }

    pub fn new_empty(data_packet_bytes_length: u64) -> ListItem {
        ListItem::new(vec![], false, vec![], vec![], false, 0, general::get_current_datestamp(), 0, data_packet_bytes_length)
    }

    /// Serialise into bytes
    pub fn into_bytes(&mut self) -> Result<Vec<u8>, String> {
        let mut serialise = Serialise::new("ListItem");

        serialise.add_field(&mut self.unique_code, crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)?;
        serialise.add_field(&mut vec![self.protected as u8], 1)?;
        serialise.add_field(&mut self.layer, crate::MAXIMUM_LENGTH_LAYER)?;
        serialise.add_field(&mut self.description, crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?;
        serialise.add_field(&mut vec![self.drop_on_retrieval as u8], 1)?;
        serialise.add_field(&mut self.creation_time_stamp, crate::LENGTH_OF_DATESTAMP_FIELD as usize)?;
        serialise.add_field(&mut self.auto_deletion_time_stamp, crate::LENGTH_OF_DATESTAMP_FIELD as usize)?;
        serialise.add_field(&mut self.downloads_count.to_be_bytes().to_vec(), crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?;
        serialise.add_field(&mut self.bytes_length.to_be_bytes().to_vec(), crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?;

        Ok(serialise.into_bytes())
    }

    /// Serialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>) -> Result<ListItem, String> {
        let mut deserialiser = Deserialise::new(&bytes, "ListItem");

        let list_item = ListItem {
            length: general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?),
            unique_code: general::strip_zeros(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_OF_UNIQUE_CODE)?),
            protected: deserialiser.extract_field(1)?[0] as u8 == 1,
            layer: general::strip_zeros(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_LAYER)?),
            description: general::strip_zeros(&deserialiser.extract_field(crate::MAXIMUM_LENGTH_DESCRIPTION_FIELD)?),
            drop_on_retrieval: deserialiser.extract_field(1)?[0] as u8 == 1,
            creation_time_stamp: deserialiser.extract_field(crate::LENGTH_OF_DATESTAMP_FIELD as usize)?,
            auto_deletion_time_stamp: deserialiser.extract_field(crate::LENGTH_OF_DATESTAMP_FIELD as usize)?,
            downloads_count: general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?),
            bytes_length: general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?)
        };

        Ok(list_item)
    }
}

/// A struct that is used to request a list of all uploads waiting on the server for a specific access_key
/// and also to send that list over the wire
#[derive(Clone, Debug)]
pub struct ListUploads {
    pub length: u64,
    pub access_key: Vec<u8>,
    pub layer: Vec<u8>,
    pub list: Vec<ListItem>
} 
impl ListUploads {
    pub fn new(access_key: &Vec<u8>, layer: &Vec<u8>, list: &Vec<ListItem>) -> ListUploads {
        ListUploads {
            length: 0,
            access_key: access_key.to_owned(),
            layer: layer.to_owned(),
            list: list.to_owned()
        }
    }

    /// Serialise into bytes
    pub fn into_bytes(&self) -> Result<Vec<u8>, String> {
        //let mut new_vector: Vec<u8> = vec![];
        let mut new_list_vector: Vec<u8> = vec![];

        //let mut length_of_list: usize = 0;
        for list_item in self.list.to_owned().iter_mut() {
            let mut serialised = list_item.into_bytes()?;
            //length_of_list += serialised.len();
            new_list_vector.append(&mut serialised);
        }

        let mut serialise = super::serialisation::Serialise::new("ListUploads");
        serialise.add_field(&mut general::append_vector_to_length(self.access_key.to_owned(), crate::MAXIMUM_LENGTH_ACCESS_KEY), crate::MAXIMUM_LENGTH_ACCESS_KEY)?;
        serialise.add_field(&mut general::append_vector_to_length(self.layer.to_owned(), crate::MAXIMUM_LENGTH_LAYER), crate::MAXIMUM_LENGTH_LAYER)?;
        if new_list_vector.len() > 0 { serialise.add_field(&mut new_list_vector, 0)?; };

        // let mut length: Vec<u8> = (crate::LENGTH_OF_DATA_PACKET_HEADER as u64 + crate::MAXIMUM_LENGTH_ACCESS_KEY as u64 + crate::MAXIMUM_LENGTH_LAYER as u64 + length_of_list as u64).to_be_bytes().to_vec();
        // new_vector.append(&mut length);
        // new_vector.append(&mut general::append_vector_to_length(self.access_key.to_owned(), crate::MAXIMUM_LENGTH_ACCESS_KEY));
        // new_vector.append(&mut general::append_vector_to_length(self.layer.to_owned(), crate::MAXIMUM_LENGTH_LAYER));
        // if new_list_vector.len() > 0 { new_vector.append(&mut new_list_vector); };

        Ok(serialise.into_bytes())
    }

    /// Deserialise from bytes
    pub fn from_bytes(bytes: Vec<u8>) -> Result<ListUploads, String> {
        let mut deserialiser = Deserialise::new(&bytes, "ListItem");
        let mut list_uploads = ListUploads {
            length: general::get_64bit_number_from(&deserialiser.extract_field(crate::LENGTH_OF_U64_NUMBER_IN_BYTES)?),
            access_key: deserialiser.extract_field(crate::MAXIMUM_LENGTH_ACCESS_KEY)?,
            layer: deserialiser.extract_field(crate::MAXIMUM_LENGTH_LAYER)?,
            list: vec![]
        };
        // Are there items in the list?
        let list_bytes = deserialiser.extract_field_to_end()?;
        if list_bytes.len() > 0 {
            let mut offset_of_item = crate::LENGTH_OF_U64_NUMBER_IN_BYTES + crate::MAXIMUM_LENGTH_ACCESS_KEY + crate::MAXIMUM_LENGTH_LAYER;
            loop {
                let length_of_item = general::get_64bit_number_from(&bytes[offset_of_item as usize..bytes.len()].to_vec()) as usize;
                let item_bytes = bytes[offset_of_item as usize..offset_of_item + length_of_item as usize].to_vec();
                let result_item = ListItem::from_bytes(&item_bytes)?;
                list_uploads.list.push(result_item);
                offset_of_item += length_of_item;
                if offset_of_item >= bytes.len() {
                    break;
                }
            }
        }

        Ok(list_uploads)
    }
}

/// Used to retreive a public key from the server
#[derive(Debug)]
pub struct RsaPublicKey {
    pub length: u64,
    pub packet_type: super::enums::PacketType,
    pub rsa_key_length: super::enums::RsaKeyLength,
    pub bytes: Vec<u8>,
}
impl RsaPublicKey {
    /// Create a new request or response for a RSA public key
    pub fn new(rsa_key_length: super::enums::RsaKeyLength, bytes: Vec<u8>) -> RsaPublicKey {
        RsaPublicKey {
            length: 0,
            packet_type: enums::PacketType::RsaPublicKeyGeneric,
            rsa_key_length: rsa_key_length,
            bytes: bytes,
        }
    }
    /// Serialise into bytes
    pub fn into_bytes(&self) -> Result<Vec<u8>, String> {
        let mut new_vector: Vec<u8> = vec![];

        // Length must be first in Vector
        let mut length: Vec<u8> = (crate::LENGTH_OF_DATA_PACKET_HEADER as u64 + crate::LENGTH_OF_U16_NUMBER_IN_BYTES as u64 + self.bytes.len() as u64).to_be_bytes().to_vec();
        let mut rsa_key_length: Vec<u8> = (self.rsa_key_length as u16).to_be_bytes().to_vec();

        new_vector.append(&mut length);
        new_vector.push(self.packet_type as u8);
        new_vector.append(&mut rsa_key_length);
        new_vector.append(&mut self.bytes.to_owned());
        
        Ok(new_vector)
    }

    /// Deserialise from bytes
    pub fn from_bytes(bytes: &Vec<u8>) -> RsaPublicKey {
        let r = RsaPublicKey {
            length: bytes.len() as u64,
            packet_type: super::enums::PacketType::from_byte(&bytes[crate::LENGTH_OF_U64_NUMBER_IN_BYTES as usize]),
            rsa_key_length: super::enums::RsaKeyLength::from(
                super::general::get_16bit_number_from(&bytes[crate::LENGTH_OF_DATA_PACKET_HEADER as usize..(crate::LENGTH_OF_DATA_PACKET_HEADER as usize + crate::LENGTH_OF_U16_NUMBER_IN_BYTES as usize)].to_vec()),
            ),
            bytes: bytes[(crate::LENGTH_OF_DATA_PACKET_HEADER as usize + crate::LENGTH_OF_U16_NUMBER_IN_BYTES as usize)..bytes.len()].to_vec(),
        };

        r
    }
}