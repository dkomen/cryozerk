/*
    cryozerk, cryozerk_server and lib_cryozerk_streams Copyright (C) 2020  Dimension15
    Authors {
      {"Name": "Dean Komen", "Contact": "www.dimension15.co.za"}      
    }

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 1 of the Dimension 15 (PTY) LTD General
    Public License as published by Dimension 15 (PTY) LTD.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 1
    of the Dimension 15 (PTY) LTD General Public License for more details.

    You should have received a copy of the  Dimension 15 (PTY) LTD 
    General Public License along with this program. If not, 
    see <https://www.dimension15.co.za> under the licenses section.
*/

//! Callback functions are passed in these structs between this main processing library and a server application

use std::net::IpAddr;
use crate::structs::{configuration, InitialHandshake, DataPacket, packet_data_types};

#[derive(Clone)]
pub struct MessageHandlers {
    pub on_new_connection: fn(&InitialHandshake)-> Result<(configuration::ServerAccess, usize), String>, 
    pub on_arrived: fn(DataPacket, InitialHandshake) -> Result<packet_data_types::UploadResponse, String>,
    pub on_update: fn(DataPacket, InitialHandshake) -> Result<packet_data_types::UploadResponse, String>,
    pub on_retrieve: fn(Vec<u8>, Vec<u8>, Vec<u8>, Vec<u8>, Vec<u8>) -> Result<(String, Vec<u8>), String>,
    pub on_packet_details: fn(Vec<u8>, Vec<u8>) -> Result<packet_data_types::PacketDetails, String>,
    pub on_delete: fn(Vec<u8>, Vec<u8>, Vec<u8>, Vec<u8>) -> Option<String>,
    pub on_timedout_delete: fn(Vec<u8>, &i64) -> Option<String>,
    pub on_message: fn(&Vec<u8>, &packet_data_types::Message) -> Result<(String, Vec<u8>), String>,
    pub on_list_uploads: fn(&Vec<u8>, &Vec<u8>) -> Result<packet_data_types::ListUploads, String>,
}

#[derive(Clone)]
pub struct BanningHandlers {
    pub on_check_if_banned: fn(&IpAddr, &Option<Vec<u8>>) -> bool,
    pub on_update_ban_details: fn(&IpAddr, &Option<Vec<u8>>)
}