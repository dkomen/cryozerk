![cryozerk Logo](https://www.dimension15.co.za/assets/images/cryozerk/cryozerk.png)

# CryoZerk and CryoZerkServer v3.1.4

Securely transfer an encrypted file (Ipv4 or Ipv6) between Linux, Windows, FreeBSD and macOS **(RSA4096 Public/Private Keys and AES256)**.
Think of it as copying a file over the network to anywhere in the world with strong end-to-end encryption, or just in your local company or home network if you wish.

- Inspired by the likes of warpinator and wormhole
- No heavy installation files, just run a single executable file for the server and one for the client
- Written with high-speed pure Rust

You can create a small private self-use server for your own file backups or for multiple seperate communities of friends and colleagues with different settings per community.

Server can be set to run in Dark Mode in which no logging of any kind is persisted.

**Data encryption passwords never leave the client**

## Website

Extensive details on the [CryoZerk website](https://www.dimension15.co.za/products/cryozerk)

## Latest downloads

- [Linux 64bit](https://www.dimension15.co.za/cryozerk/downloads/Lin64-CryoZerk.zip) - built on Debian 10
- [Windows 64bit](https://www.dimension15.co.za/cryozerk/downloads/Win64-CryoZerk.zip) - built on Windows 10
- FreeBSD, OpenBSD, macOS: You need to build CryoZerk from source

## Build from source

- Install [Rust](https://www.rust-lang.org/tools/install)
- Change to a local directory you wish then: `git clone https://gitlab.com/dkomen/cryozerk` (from this site)
- Change into new cryozerk directory
- Change to cryozerk sub-directory and execute: cargo build --release
- Change to cryozerk_server sub-directory and execute: cargo build --release
- Binaries will be in /target/release sub-directories

**Solutions to common build errors**

- [Windows VCRUNTIME140.DLL missing error](https://geeksadvice.com/fix-vcruntime140-dll-is-missing-error-on-windows/) (eg: Install the Microsoft Visual C++ 2015 Redistributable)
- [Linux glibc version error](https://benohead.com/blog/2015/01/28/linux-check-glibc-version/) - You will have to build CryoZerk from source

**One-liner Linux build**

- cd cryozerk && cargo b --release && cd ../cryozerk_server && cargo b --release && cd ..

## Basic usage

In short, you typically use only four or five commands after registration: `send`/`get`/`update` and `list`/`delete`.

There are various settings in the configuration files that you may change. For instance, on the server, the maximum file size allowed to be sent or the allowed maximum server useage disk space useage per CryoZerk community (per `access_key` in configuration files), or disable all logging with one toggled setting.

### Installation

[Instructions on Website](https://www.dimension15.co.za/products/cryozerk)
See website for installation, configuration settings and usage

#### Commands

To send a file:

`cryozerk send -f myphoto.jpg -p My58ScrtwrdS`

Your password must consist of 12 to 256 alphanumeric characters [A..Z, a..z, 0..9]. If you dont add a password a long random password will be generated for you:

`cryozerk send -f myphoto.jpg`

Alternatively you can specify `-p` without any password and CryoZerk will ask you to enter your password before sending the file, at this point additional characters may be used, examples : `" ! @ # $ % &`

You will receive a download code. A different client in the same CryoZerk community can do a `list` and see this newly available file to download, or you can send the the code in some OOB manner like Telegram or Signal.

The current server config settings allow a maximum file storage of 10GiB per access_key (you may add many access_keys) and indivdual upload 500MiB (including space used by serialisation and encryption) and 1 year before being time-out and deleted if not already deleted when downloaded.

However you can change these settings to be 10 minutes or 2 years or eternity (`"automatic_deletion_after_seconds": 0`), whatever you desire. Files on the server are deleted as soon as a download (`get`) was done unless the sender that uploaded the file optionally set the `-x` argument.

Lets say the received code is cryptodW591

To retrieve an encrypted file again:

`cryozerk get -c cryptodW591 -p My58ScrtwrdS`

The retrieved file will appear in the same directory you were in when you ran the cryozerk app.

## Privacy

No client PC specific information is logged except the **time** when any interactions were performed, the **source IP address** (to help prevent abuse of the server) and description of an upload and layer (aka. virtual file path) if it was supplied, the actual file name and its' contents are encrypted. General logging to file on both the client and the server can be disabled in the configuration files. IP Addess monitoring for abuse can not be disabled but IP addresses are only logged to database if abuse is detected.

[About password strength](https://www.inetsolution.com/blog/june-2012/complex-passwords-harder-to-crack,-but-it-may-not)

Please **note** that your command-line history may be saved to your computer - as in a bash terminal. Altenatively create an alias or bash script or if using Windows create a *.cmd file with your requirements, or use `cryozerk send -f Myfile.pdf -p` with no password and you will then be asked for a password.

## Tests

### cryozerk server

Oracle Linux 8, CentOS 8 Stream, Rocky Linux 8 Pre-release, Ubuntu Server 20.04 LTS, Debian 10, OpenSUSE Tumbleweed, GeckoLinux Rolling, Linux Mint 20, PopOs 20.10, Windows 10 Pro, OpenBSD 6.9 (using MongoDB 3.3.2)

### cryozerk client

Linux Mint 20, Windows 10 Pro (progress bars are disabled on Windows), PopOs 20.10, Manjaro, OpenSUSE Tumbleweed, GeckoLinux Rolling, Oracle Linux 8, Centos Stream, Ubuntu Server 20.04 LTS, Debian 10, LMDE 4, FreeBSD 12.2, OpenBSD 6.9

Although Windows clients work fine, a better experiance is gained with '*nix' clients.

### MongoDB

MongoDB Community Edition 4.4.2+ ([install](https://docs.mongodb.com/manual/installation/) it and make sure its up and running, CryoZerk Server will start using it)

**IPv4 White listing** IP address white listing is allowed per access_key and allows you to additionaly limit access to specific subnets or specific IP addresses

White listing example - allow local only:

    "access_keys": [
    {
        "access_key": "PrivateAccess",
        "ip_address_white_list": [ { "ip_address": "127.0.0.1" }checks
    {
        "access_key": "HomeAndFromWork",
        "ip_address_white_list": [
            { "ip_address": "192.168.0.[1-255]" },
            { "ip_address": "82.103.129.71" }
        ]

## Change log

**3.1.3**
- Ability to run the *nix client with the install option: `sudo cryozerk install`

**3.1.2**
- Updated tokio runtime from v0.2 to v1.4

**3.1.0 (Not backward compatible)**
- Server can require a client to supply an additional registration key before communications are allowed
- Optional additional security feature for secure specific-client to specific-client by employing a shared secret of random bytes. Requires a RSA 4096 public-key exchange between clients, the server still connects the two (or more) clients
- Setting per community to allow custom unique codes for uploads and therefore downloads
- Hash verification: Ensure file data is not changed by generating a SHA512 hash on the sending client (`send`) and verify the hash on the receiving client (`get`). Option to disable with argument `--nohash`
- General refactoring

**2.2.6**
- New setting for cryozerk_server dark mode: All logging of any kind is disabled if true (normal file logging and ban logging to database(banning is still active in RAM though))

You will need to manually modify the cz_server.json file to include the "dark_mode" setting... or rebuild it with default settings by deleting it and re-running the 'cryozerk_server' application

        ...
        "second_ban_time_in_seconds": 21600,
        "logging_enabled": true,
        "dark_mode": false
    }

**2.2.5**
- Progress bars now display on Windows 10 too

**2.2.4**
- Due to Rust 1.51 release - changed panic! format to remove warning (see non-fmt-panic)

**2.2.3**
- Additional random words for unique_code's
- Longer random padding lengths for small files

**2.2.2**
- Minor refactoring

**2.2.1**
- Fixed incorrect message displayed after a delete was done
- Fixed a sometimes occuring bug where a downloaded file's filename may lose a character or two.

**2.2.0 (Not backward compatible)**
- Added new command `update` to update existing uploaded file data with new data
- Command-line argument `-xx` renamed to `-px`

**2.1.0 (Not backward compatible)**
- Stronger 512-bit AES encryption salt
- Fixed bug when creating a new thread in an existing tokio runtime
- Updated Rust's mongodb driver from v1.1.1 to v1.2
- Command-line argument `-x1` renamed to `-x`
- Command-line argument `-x2` renamed to `-xx`

**2.0.1 (Not backward compatible)**
- File encryption\decrytion on clients are now multi-threaded
- Obfuscation by adding random padding bytes to File data packets
- Ability to protect an upload with an additional password which is needed if a file is to be deleted `-x2`
- Longer password cypher used for verification before downloads and deletes (also deletes using `-x2`)
- Changed default upload layer to '/' if none was specified
- Stronger header scrambler implementation
- Fewer progress bars displayed
- Longer random session keys
- Other minor changes

## Ongoing

- Code refactoring
- A website with information and links to all documentation: [In progress here](https://www.dimension15.co.za/products/cryozerk)
- A video on YouTube on how to use the client, server installation (as a service on Linux) and MongoDB install

# Free use and modification

This program is free software: you can redistribute it and/or modify it under the terms of the Dimension 15 (PTY) LTD General Public License v1 as published by Dimension 15 (PTY) LTD.

See www.dimension15.co.za for detailed public licence.

You may however charge a fee for your resource useage in giving clients this service (bandwidth, RAM, CPU and HDD space)

**Any paypal donations to dkomen@protomail.com or support at www.patreon.com/dkomen will be truly appreciated**
**For issues or comments send an email to: incoming+dkomen-cryozerk-21979842-issue-@incoming.gitlab.com**
